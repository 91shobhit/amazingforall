<?php

namespace Drupal\custom_dev\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides routes response for the Custom_Dev module.
 */
class DevelopmentController extends ControllerBase {
	
	/**
	* Returns a simple page
	* @return array
	*/
	public function content() {
      $element = array(
        '#markup' => 'Welcome Developers.',
      );
	  
      return $element + $this->renderCustomForm();
	}

	public function renderCustomForm() {
      return $form = \Drupal::formBuilder()->getForm('\Drupal\custom_dev\Form\CustomForm');
  }

}