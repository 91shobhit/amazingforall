<?php
/**
 * @file
 * Contains \Drupal\custom_routes\Controller\ExampleController.
 */
namespace Drupal\custom_routes\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * App controller
 */
class ExampleController extends ControllerBase {
	/**
	 * {@inheritdoc}
	 */
	public function content() {
		
		try {
		  /* $credentials = base64_encode('demo:demo');
		  $uri = 'http://rfbdwn0yjw4ggoox.myfritz.net:88/engine-rest/task?processInstanceId=a910740b-1263-11e8-868a-b8975af3555d';
          $client  = \Drupal::httpClient()->get($uri, array('headers' => array('Accept' => 'application/json'), 'auth' => array( 'demo', 'demo')));
          $data = (string) $client->getBody();
          if (empty($data)) {
            watchdog_exception('custom_routes','data is empty');

            return FALSE;
          } */
		  $data = $this->postcontent();
          $build = [
			'#markup' => $data,
		  ];
		  return $build;
        }
        catch (RequestException $e) {
              watchdog_exception('custom_routes', $e->getMessage());
		  return FALSE;
        }
		
	}
    public function postcontent() {
        $client = \Drupal::httpClient();
	    $request = $client->post('http://rfbdwn0yjw4ggoox.myfritz.net:88/engine-rest/process-definition/key/FormsFlow/start', [
		   "formName" => "form1",
		  "formInputPhoneEmail"=> "004915775786792"
		]);
	    $response = json_decode($request->getBody());
        return $response;
    }
}